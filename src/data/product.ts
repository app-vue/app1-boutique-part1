export default [
  {
    id: 1,
    image: "src/assets/images/macbook.jpg",
    title: "MacBook Pro 1",
    description: "Lorem ipsum dolor sit amet.",
    price: 1500,
  },
  {
    id: 2,
    image: "src/assets/images/macbook.jpg",
    title: "MacBook Pro 2",
    description: "Lorem ipsum dolor sit amet.",
    price: 1500,
  },
  {
    id: 3,
    image: "src/assets/images/macbook.jpg",
    title: "MacBook Pro 3",
    description: "Lorem ipsum dolor sit amet.",
    price: 1500,
  },
  {
    id: 4,
    image: "src/assets/images/macbook.jpg",
    title: "MacBook Pro 4",
    description: "Lorem ipsum dolor sit amet.",
    price: 1500,
  },
  {
    id: 5,
    image: "src/assets/images/macbook.jpg",
    title: "MacBook Pro 5",
    description: "Lorem ipsum dolor sit amet.",
    price: 1500,
  },
];
